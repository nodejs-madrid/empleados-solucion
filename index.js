const debug = require('debug')('empleados:main');
const program = require('commander');
const inquirer = require('inquirer');
const getEmpleados = require('./api');

program.option('-f, --full', 'Todos los datos').parse(process.argv);

let isFull;

if (program.full) {
    isFull = true;
    debug('Recibido full');
    getEmpleados(isFull);
} else {
    debug('Sin parámetros');
    prompt = inquirer.createPromptModule();
    prompt([
        {
            type: 'confirm',
            name: 'full',
            message: '¿Quieres imprimir todos los datos?'
        }
    ]).then(respuesta => {
        isFull = respuesta.full;
        getEmpleados(isFull);
    });
}

