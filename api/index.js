const debug = require('debug')('empleados:api');
const axios = require('axios');

const urlApi = 'http://dummy.restapiexample.com/api/v1/employees';

module.exports = function getEmpleados(isFull) {
    debug('Conectando a la API');
    axios.get(urlApi).then(datos => {
        const empleados = datos.data;
        debug('Recibida respuesta de la API');
        const totalEmpleados = empleados.length;
        let edadMedia = 0;
        let salarioMedio = 0;
        empleados.forEach(empleado => {
            edadMedia += empleado.employee_age / totalEmpleados;
            salarioMedio += empleado.employee_salary / totalEmpleados;
        });
        console.log(`
            Total:         ${totalEmpleados} empleados
            Edad media:    ${edadMedia.toFixed(2)} años
            Salario medio: ${salarioMedio.toFixed(2)} rublos / mes`);
        if (isFull) {
            console.log(`
            
            Empleados:
            `, empleados.slice(0, 10).map(empleado => {
                    let empleadoFormat = {
                        nombre: empleado.employee_name,
                        edad: empleado.employee_age,
                        salario: empleado.employee_salary
                    }
                    return empleadoFormat;
                }));
        }
    }).catch(err => {
        debug("Error al conectar a la API: " + err.message);
    });
};